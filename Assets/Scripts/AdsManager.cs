﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using System.Collections;
using System.Collections.Generic;

public class AdsManager : MonoBehaviour 
{
    public Text message;
    public bool AdColonyReady = false;
    public bool VungleReady = false;
    public bool UnityAdsReady = false;

    public enum AdPlugin
    {
        AdColony = 0,
        Vungle = 1,
        UnityAds = 2
    }

    public AdPlugin adPlugin = AdPlugin.AdColony;

    void Start()
    {
        InitializeAdColony();
        InitializeVungle();
        InitializeUnityAds();
    }

    void Update()
    {
        if (AdColony.IsVideoAvailable(adColonyZoneID) && !AdColonyReady)
        {
            AdColonyReady = true;
            GameObject.Find("AdColony").GetComponent<Image>().color = Color.green;
        }
        if (Vungle.isAdvertAvailable() && !VungleReady)
        {
            VungleReady = true;
            GameObject.Find("Vungle").GetComponent<Image>().color = Color.green;
        }
        if (Advertisement.IsReady("defaultZone") && !UnityAdsReady)
        {
            UnityAdsReady = true;
            GameObject.Find("UnityAds").GetComponent<Image>().color = Color.green;
        }
    }

    public void PlayVideoAd()
    {
        switch (adPlugin)
        {
            case AdPlugin.AdColony:
                PlayAdColonyVideo();
                break;
            case AdPlugin.Vungle:
                PlayVungleVideo();
                break;
            case AdPlugin.UnityAds:
                break;
            default:
                break;
        }
    }

    #region AdColony Methods/Variables

#if UNITY_ANDROID
    private string adColonyAppID = "appa54ac11bd36042acb9";
    private string adColonyZoneID = "vzb545cee5b368488aa4"; 
#endif
#if UNITY_IOS
    private string adColonyAppID = "app3fef46b7409e417c83";
    private string adColonyZoneID = "vza1e109b64bd343e2a1";
#endif

    private void InitializeAdColony()
    {
        AdColony.OnVideoFinished = OnVideoFinished;
        AdColony.Configure
        (
            "version:1.0,store:google",
            adColonyAppID,
           adColonyZoneID
        );    
    }

    public void PlayAdColonyVideo()
    {
        if (AdColony.IsVideoAvailable(adColonyZoneID))
        {
            Debug.Log("Play AdColony Video");
            AdColony.ShowVideoAd(adColonyZoneID);
        }
        else
        {
            Debug.Log("Video Not Available");
        }
    }

    private void OnVideoFinished(bool ad_was_shown)
    {
        Debug.Log("On AdColony Video Finished");
        AdColonyReady = false;
    }
    #endregion

    #region Vungle Methods/Variable

    private void InitializeVungle()
    {
        Vungle.init("57bef7370dc8013d22000069", "57bf1f55d80d93952200000c");
        Vungle.onAdFinishedEvent += OnVungleVideoFinish; 
    }
    public void PlayVungleVideo()
    {
        if (Vungle.isAdvertAvailable())
        {
            Debug.Log("Vungle video is ready");
            message.text = "Vungle video is ready";
            Dictionary<string, object> options = new Dictionary<string, object>();
            Vungle.playAdWithOptions(options);        
        }
        else
        {            
            Debug.Log("Vungle video is not ready");
            message.text = "Vungle video is not ready";
        }
    }

    private void OnVungleVideoFinish(AdFinishedEventArgs args)
    {
        Debug.Log("On Vungle Video Finished");
        VungleReady = false;
    }

    #endregion

    #region UnityAds Methods/Variables

    private string androidGameId = "126997";
    private string iosGameId = "126996";
    private bool enableTestMode = true;

    private void InitializeUnityAds()
    {
        string gameId = null;

        #if UNITY_ANDROID
            gameId = androidGameId;
        #endif
        #if UNITY_IOS
            gameId = iosGameId;
        #endif

        if(string.IsNullOrEmpty(gameId))
        {
            Debug.LogError("Failed to initialize Unity Ads. Game ID is null or empty.");
        }
        else if (!Advertisement.isSupported)
        {
            Debug.LogWarning("Unable to initialize Unity Ads. Platform not supported.");
        }
        else if (Advertisement.isInitialized)
        {
            Debug.Log("Unity Ads is already initialized.");
        }
        else
        {
            Debug.Log(string.Format("Initialize Unity Ads using Game ID {0} with Test Mode {1}.",
                gameId, enableTestMode ? "enabled" : "disabled"));
            Advertisement.Initialize(gameId, enableTestMode);

        }
    }

    public void PlayUnityAdsVideo()
    {
        if (Advertisement.IsReady("defaultZone"))
        {
            ShowOptions options = new ShowOptions();
            options.resultCallback = HandleShowResult;
            Advertisement.Show("defaultZone", options);

            Debug.Log("UnityAds video is ready");
            message.text = "UnityAds video is ready";
        }
        else
        {
            Debug.Log("UnityAds video is not ready");
            message.text = "UnityAds video is not ready";
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Failed:
                Debug.LogError("Video failed to show.");
                message.text = "Video failed to show.";
                break;
            case ShowResult.Skipped:
                Debug.LogWarning("Video was skipped.");
                message.text = "Video was skipped.";
                break;
            case ShowResult.Finished:
                UnityAdsReady = false;
                Debug.Log("UnityAds video completed.");
                message.text = "UnityAds video completed.";
                break;
            default:
                break;
        }
    }

    #endregion

}
